# Gridy Grid Jquery Theme

This package allows you to have Jquery UI like web components data grid

The detailed docs can be found here: [gridy-grid](https://www.npmjs.com/package/gridy-grid)

Gridy Grid uses Skinny Widgets library, so you can go with wider number of elements: 
[skinny-widgets](https://www.npmjs.com/package/skinny-widgets)

## Installation

```
npm i gridy-grid gridy-grid-jquery sk-theme-jquery --save
```

## Usage

```html
<gridy-table theme="jquery" id="gridyTable"></gridy-table>

<script type="module">
    import { GridyTable } from '/node_modules/gridy-grid/src/table/gridy-table.js';
    import { DataSourceLocal } from '/node_modules/gridy-grid/src/datasource/data-source-local.js';
    let dataSource = new DataSourceLocal();
    dataSource.fields = [
        { title: 'Title', path: '$.title' },
        { title: 'Price', path: '$.price' }
    ];
    let data = [];
    for (let i = 0; i < 10; i++) {
        data.push({ title: 'row' + i, price: 100 * i })
    }
    // local datasource data load should be called explicitly
    gridyTable.dataSource = dataSource;
    customElements.define('gridy-table', GridyTable);
    dataSource.loadData(data);
</script>
```
