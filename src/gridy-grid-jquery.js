

import { GridyGridImpl } from "../../gridy-grid/src/impl/gridy-grid-impl.js";



export class GridyGridJquery extends GridyGridImpl {

    get dataTablesCssPath() {
        if (! this._dataTablesCssPath) {
            this._dataTablesCssPath = this.comp.confValOrDefault('dt-css-path',
                `/node_modules/gridy-grid-${this.comp.theme}/`) + 'jquery.dataTables.css';
        }
        return this._dataTablesCssPath;
    }

    get container() {
        if (! this._container) {
            this._container = this.comp.el.querySelector('.dataTables_wrapper');
        }
        return this._container;
    }

    get tplVars() {
        return {
            'dtCssPath' : this.comp.confValOrDefault('dt-css-path',
                `/node_modules/gridy-grid-${this.comp.theme}/`), ...this.comp.tplVars
        };
    }

    renderImpl() {
        this.logger.debug('GridyGridJquery.renderImpl()');
        super.renderImpl();
        this.comp.whenRendered(() => {
            let fileName = this.fileNameFromUrl(this.dataTablesCssPath);
            if (!this.mountedStyles[fileName]) {
                this.attachStyleByPath(this.dataTablesCssPath, this.comp);
            }
        });
    }
}