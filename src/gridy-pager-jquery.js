


import { GridyPagerImpl } from "../../gridy-grid/src/pager/impl/gridy-pager-impl.js";



export class GridyPagerJquery extends GridyPagerImpl {

    get pageBtnContainer() {
        if (! this._pageBtnContainer) {
            this._pageBtnContainer = this.comp.querySelector('.dataTables_paginate');
        }
        return this._pageBtnContainer;
    }

    set pageBtnContainer(pageBtnContainer) {
        this._pageBtnContainer = pageBtnContainer;
    }

    get subEls() {
        return [ 'pageBtnContainer' ];
    }

}